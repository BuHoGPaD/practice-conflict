export default class Person {
    constructor(name) {
        this.name = name;
    }

    say(phrase) {
        return `${this.name} says ${phrase}`;
    }

    askSmthToSmb(phrase, opponentName) {
        return `${this.name} asks "${phrase}, ${opponentName}?"`;
    }
}